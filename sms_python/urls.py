from django.conf.urls import url, include

from django.urls import path
from . import views

urlpatterns = [
    url(r"^$", views.HomePageView.as_view()),
    url(r'^envio_sms/', views.envio_sms),
    url(r'^credito/', views.creditos_disp),
]

