# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SmsPythonConfig(AppConfig):
    name = 'sms_python'
