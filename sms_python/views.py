# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie

from django.http import JsonResponse
from django.http import QueryDict

import json
import requests

# Create your views here.
# def HomePageView(request):
#     return render(request, 'index.html', {})

class HomePageView(TemplateView):
    @method_decorator(ensure_csrf_cookie)
    def get(self, r, **kwargs):
        return render(r, "index.html", context=None)

def envio_sms(request):
    apikey = request.GET.get('apikey', None)
    mensaje = request.GET.get('mensaje', None)
    numero = request.GET.get('numero', None)
    codigoPais = request.GET.get('codigoPais', None)
    targetURL = "https://api.smsmasivos.com.mx/sms/send"
    headers = {
        'apikey':apikey
    }
    data = {
        'message':mensaje,
        'numbers':numero,
        'country_code':codigoPais
    }
    r = requests.post(url = targetURL, data = data, headers = headers)

    return JsonResponse(r.text, safe=False)


def creditos_disp(request):
    token = request.GET.get('token', None)
    targetURL = "https://api.smsmasivos.com.mx/credits/consult"
    headers = {
    'token': token
    }
    r = requests.post(url = targetURL, data = {}, headers = headers)

    decoded = json.loads(r.text) 
    return JsonResponse(decoded)
