$(document).ready(function() {
  $("textarea#textarea1").characterCounter();
  $("select").formSelect();
});

$("#apiform").validate({
  rules: {
    destination: {
      required: true,
      minlength: 10
    },
    content: {
      required: true,
      maxlength: 160
    }
  },
  messages: {
    destination: {
      required: "Requerido",
      minlength: "M\u00EDnimo 10 d\u00EDgitos"
    },
    content: {
      required: "Requerido",
      maxlength: "No puede exceder 160 caracteres"
    }
  },
  errorElement: "span",
  errorPlacement: function(error, element) {
    var placement = $(element).data("error");
    if (placement) {
      $(placement).append(error);
    } else {
      error.insertAfter(element);
    }
  },
  submitHandler: function() {
    apikey = $("#api").val()
    mensaje = $("#textarea1").val()
    numero = $("#destination").val()
    codigoPais = $("#country option:selected").text()
    
      $.ajax({
        dataType: "json",
        url: "/envio_sms/",
        data: { 
          'apikey': apikey,
          'mensaje': mensaje,
          'numero': numero,
          'codigoPais': codigoPais
        },
        success: function(data) {
          console.log(data)
          var data = JSON.parse(data)
          if (data.success) {
            swal("Mensaje enviado", "", "success");
          } else {
            swal(data.message, "", "error");
          }
          $("#textarea1").val('')
          $("#destination").val('')
          
        },
        error: function() {
          console.log("error")
        }
      });
  }
});

$('#request_credits').on("click", e => {
  token = $('#token').val()
  if (token != '') {
    $.ajax({
      dataType: "json",
      url: "/credito/",
      data: { 'token': token },
      success: function(data) {
        console.log(data)
          swal("Cuentas con "+data.credit+" créditos", "", "success");    
      },
      error: function() {
        console.log("error")
      }
    });
  }else {
    swal("Necesitas generar el token primero", "", "error");
  }
});